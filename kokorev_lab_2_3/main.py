"""Работа с файлом titanic.csv. Часть 2

По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений),
в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров
два наиболее важных признака из трех рассматриваемых (по варианту).

3 Sex,Age,SibSp
"""

import pandas as pd
from sklearn.tree import DecisionTreeClassifier

COL_SURVIVED = 'Survived'
COL_SIBSP = 'SibSp'
COL_AGE = 'Age'
COL_SEX = 'Sex'
COL_PASS_ID = 'PassengerId'


def csv_read(file):
    """читаем таблицу"""
    return pd.read_csv(file, index_col=COL_PASS_ID)


def clear_empty_values(data_set):
    """удаляем пустые значения"""
    return data_set[[COL_AGE, COL_SEX, COL_SIBSP, COL_SURVIVED]].dropna()


def replace_values(data_set):
    """заменяем обозначения полов на 0 и 1"""
    return data_set[[COL_AGE, COL_SEX, COL_SIBSP]].replace("female", 0).replace("male", 1)


data = csv_read('titanic.csv')
data_frame = pd.DataFrame(data=data, columns=[COL_AGE, COL_SEX, COL_SIBSP, COL_SURVIVED])
data_frame = clear_empty_values(data_frame)

X = replace_values(data_frame)
Y = data_frame[COL_SURVIVED]

clf = DecisionTreeClassifier()  # классификатор дерева решений
clf.fit(X, Y)  # построение дерева решений

print("рассматриваемые признаки: ")
print(clf.feature_names_in_)

print("важность признаков: ")
print(list(map(lambda x: round(x, 5), clf.feature_importances_)))
