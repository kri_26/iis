from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.model_selection import train_test_split

from pikov_lab_3_16.Constants import X_DATA, Y_DATA, SHARE_OF_TEST_DATASET, X_TEST_DATA, Y_TEST_DATA, Y_TRAINING_DATA, \
    X_TRAINING_DATA, DOT_Y_COORD, PLOTS_COLORMAP, PLOTS_ROWS, PLOTS_COLUMNS, TEST_DATASET_DOTS_PLOT, DOT_X_COORD, \
    TEST_DATASET_PREDICTION_DOTS_PLOT, TEST_DATASET_CLASS_PLOT, TEST_Y_COLOR, TEST_DATASET_PREDICTION_CLASS_PLOT, \
    PREDICTED_Y_COLOR, PLOT_TITLE_SIZE


class ModelAnalyzer:

    def __init__(self, data):
        datasets = divide_to_training_and_test_datasets(data)
        self._training_dataset = extract_training_dataset(datasets)
        self._test_dataset = extract_test_dataset(datasets)

    def analyze(self, model):
        self.train_model(model)
        self.show_plot_test_dataset_with_predictions_of_model(model)
        self.print_characteristics_of_model(model)

    def train_model(self, model):
        x = self._training_dataset[X_DATA]
        y = self._training_dataset[Y_DATA]
        model.fit(x, y)

    def show_plot_test_dataset_with_predictions_of_model(self, model):
        model_name = type(model).__name__
        fig = plt.figure()
        fig.suptitle(model_name, fontsize=PLOT_TITLE_SIZE)

        self.create_test_dataset_dots_subplot()
        self.create_test_predictions_dots_subplot(model)
        self.create_test_dataset_class_subplot()
        self.create_test_predictions_class_subplot(model)

        plt.show()

    def create_test_dataset_dots_subplot(self):
        x_test = self._test_dataset[X_DATA]
        y_test = self._test_dataset[Y_DATA]

        plt_test_dataset = plt.subplot(PLOTS_ROWS, PLOTS_COLUMNS, TEST_DATASET_DOTS_PLOT)
        plt_test_dataset.scatter(
            x_test[:, DOT_X_COORD],
            x_test[:, DOT_Y_COORD],
            c=y_test,
            cmap=PLOTS_COLORMAP
        )

    def create_test_predictions_dots_subplot(self, model):
        x_test = self._test_dataset[X_DATA]
        y_predicted = model.predict(x_test)

        plt_test_predict = plt.subplot(PLOTS_ROWS, PLOTS_COLUMNS, TEST_DATASET_PREDICTION_DOTS_PLOT)
        plt_test_predict.scatter(
            x_test[:, DOT_X_COORD],
            x_test[:, DOT_Y_COORD],
            c=y_predicted,
            cmap=PLOTS_COLORMAP
        )

    def create_test_dataset_class_subplot(self):
        x_test = self._test_dataset[X_DATA]
        y_test = self._test_dataset[Y_DATA]

        plt_test = plt.subplot(PLOTS_ROWS, PLOTS_COLUMNS, TEST_DATASET_CLASS_PLOT)
        plt_test.scatter(
            x_test[:, DOT_X_COORD],
            y_test,
            color=TEST_Y_COLOR
        )

    def create_test_predictions_class_subplot(self, model):
        x_test = self._test_dataset[X_DATA]
        y_predicted = model.predict(x_test)

        plt_test_predict = plt.subplot(PLOTS_ROWS, PLOTS_COLUMNS, TEST_DATASET_PREDICTION_CLASS_PLOT)
        plt_test_predict.scatter(
            x_test[:, DOT_X_COORD],
            y_predicted,
            color=PREDICTED_Y_COLOR
        )

    def print_characteristics_of_model(self, model):
        x_test = self._test_dataset[X_DATA]
        y_test = self._test_dataset[Y_DATA]
        predictions = model.predict(x_test)

        print('Mean absolute error=', metrics.mean_absolute_error(y_test, predictions))
        print('Mean squared error=', metrics.mean_squared_error(y_test, predictions))


def divide_to_training_and_test_datasets(data):
    return train_test_split(
        data[X_DATA],
        data[Y_DATA],
        test_size=SHARE_OF_TEST_DATASET
    )


def extract_test_dataset(datasets):
    return [datasets[X_TEST_DATA], datasets[Y_TEST_DATA]]


def extract_training_dataset(datasets):
    return [datasets[X_TRAINING_DATA], datasets[Y_TRAINING_DATA]]
