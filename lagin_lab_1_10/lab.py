import pandas as pd


# производит поиск строки csv файла в которой значение ячейки столбца Age будет наименьшим
# и возвращает значение ячейки столбца Name этой строки

def get_min_age_human_name() -> str:
    data = pd.read_csv("titanic.csv")
    ageList = data["Age"]
    nameList = data["Name"]

    minAge = ageList[0]
    minAgeIndex = 0
    for i in range(1, ageList.count() - 1):
        if ageList[i] < minAge:
            minAge = ageList[i]
            minAgeIndex = i

    return nameList[minAgeIndex]


print(get_min_age_human_name())
