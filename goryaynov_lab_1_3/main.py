import pandas
from pandas import DataFrame

'''
Какую долю пассажиры первого класса составляли среди всех пассажиров? 
Ответ приведите в процентах 
(число в интервале от 0 до 100, знак процента не нужен), 
округлив до двух знаков. 
'''

def load_data(path: str) -> DataFrame:
    return pandas.read_csv(path)

def get_percent(part: int, total: int) -> float:
    percentage = part / total * 100
    return round(percentage, 2)

data: DataFrame = load_data('titanic.csv')
first_class_count = 0
total_count = 0
for _, row in data.iterrows():
    total_count += 1
    if str(row['Pclass']) == '1':
        first_class_count += 1
percentage = get_percent(first_class_count, total_count)
print(percentage)