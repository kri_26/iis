"""7. Какое самое популярное мужское имя на корабле?"""

import pandas as p
import re

data = p.read_csv('titanic.csv')

def delete_surname(name):
 
    # Первое слово до запятой - фамилия
    s = re.search('^[^,]+, (.*)', name)
    if s:
        name = s.group(1)
 
    # Удаляем обращения
    name = re.sub('(Don\. |Mr\. |Master\. |Rev\. |Dr\. |Major\. |Sir\. )', '', name)
    return name
 
names = data[data['Sex'] == 'male']['Name'].map(delete_surname)
name_counts = names.value_counts()
print(name_counts.head(1))
