import pandas as pd
#9. Найдите имя самого пожилого человека
def older_person() -> str:
    data = pd.read_csv("titanic.csv")
    listAge = data["Age"]
    listName = data["Name"]

    maxAgeIndex = 0
    maxAge = listAge[0]
    for i in range(listAge.count()):
        if listAge[i] > maxAge:
            maxAge = listAge[i]
            maxAgeIndex = i

    return listName[maxAgeIndex]

print(older_person())
