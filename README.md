# Интеллектуальные информационные системы

Репозиторий для лабораторных работ студентов групп ПИбд-41 (2021, осень).

## Установка репозитория.

1. Отправьте запрос на вступление в репозиторий https://gitlab.com/Alexeyyy/iis/-/project_members в GitLab.

2. Установите git на компьютере
```
Скачать отсюда https://git-scm.com/
```

3. Склонируйте репозиторий на компьютер
```
git clone https://gitlab.com/Alexeyyy/iis.git
```

## Перед выполнением каждой лабораторной работы сделайте это:

1. Переключитесь на ветку main (например, ранее выполнялась другая л/р -> вот нужно вернуться в корневую ветку)
```
git checkout main
```

2. Втяните все изменения к себе (эти изменения, те лабораторные работы, что я принял и вмержил в main-ветку)
```
git pull
```

3. Создайте ветку своей очередной лабораторной работы (при этом проверьте, что вы в ветке main находитесь, проверить так: ```git status```)

```
git checkout -b <фамилия>_lab_<номер л/р>_<вариант>

пример: git checkout -b zhelepov_lab_1_11
```

4. Переключитесь на новую ветку или проверьте, что уже на ней.

```
git checkout <фамилия>_lab_<номер>__<вариант>
```

5. Создайте папку в корне репозитория, название папки соответствует названию только что созданной ветки.
```
-- Создать папку (при этом проверьте, что находитесь в своей ветке через git status)
mkdir zhelepov_lab_1_11

-- Зайти в папку
cd zhelepov_lab_1_11
```

<p>
    <strong>В папке размещаются все необходимые материалы по лабораторной работе:</strong>
</p>
<ul>
    <li>отчет в формате .pdf (отчет содержит титульный лист, вот пример: )</li>
    <li>файлы лабораторной работы (код, файлы с датасетами)</li>
    <li>readme.md файл, который содержит ссылку на видео того, как функционирует ваша лабораторная работа</li>
</ul>

**Внимание:** лабораторные работы, названные не в указанном формате, файлы которых сложены не в соответствующей папке, а также которые не содержат отчета и readme.md с ссылкой на видео работы, файлов с кодом и датасетами не проверяются!!!

6. В процессе подготовки лабораторной работы можно делать неограниченное число коммитов, при этом коммитить можно только в свою ветку.
Команды для создания коммита изменений
```
-- посмотреть перечень изменненных файлов
git status

-- добавить все изменения для нового коммит.
git add .

-- добавить конкретный файл для нового коммита.
git add <путь до файла, который можно посмотреть через git status>

-- добавить коммит
git commit -m "Мой новый коммит"
```

6. Сохраните изменения в репозиторий на GitLab. Лучше это делать после выполнения лабораторной работы.
```
git push origin
```

7. Создайте мерж-реквест в GitLab. Вот видео, как это сделать: https://drive.google.com/file/d/1jEASQHgTIQ2tm1G9K8FJYfrYOem-Unk-/view?usp=sharing
<p>На что обратить внимание</p>
<ul>
    <li>Ставим в assignee Alexeyyy</li>
    <li>Можно оповестить меня в нашем рабочем чате в Telegram</li>
</ul>

<p>
    Поздравляю, Вы сдали на проверку очередную лабораторную работу!
</p>

## Процедура сдачи лабораторных работ.

1. Я проверяю ваш код, отчет и видео. Вот несколько подсказок, чтобы сдать быстрее:
<ul>
    <li>форматируйте код в соответствии с code conventions https://www.python.org/dev/peps/pep-0008/ языка Python</li>
    <li>упорядочивайте свои мысли в отчете, отчет не должен быть в стиле "взрыв на макаронной фабрике", все четко: что за чем следует и почему</li>
    <li>Cтруктура отчета:
        <ul>
            <li>Титульный лист</li>
            <li>Задание на л/р</li>
            <li>Исходные данные (оформляйте в виде таблиц, если данных очень много, то оформляйте только часть данных)</li>
            <li>Выходные данные (аналогично)</li>
            <li>Описание того, что делает ваша программа (листинги с описаниями)</li>
            <li>Заключение</li>
        </ul>
    </li>
    <li>Не прикрепляйте видео-файл!!!! Выложите его в облако, YouTube и дайте ссылку в readme.md файле вашей л/р</li>
</ul>

2. Если все хорошо, то я задам (или если все очень качественно сделано, то не задам) несколько контрольных вопросов в ревью мерж-реквеста.

3. Если есть замечания по л/р, то они также будут отражены в ревью.

4. Отвечаете на контрольные вопросы / исправляете замечания. Если замечания касательно вашего кода, файлов л/р, то можно докоммитить новые изменения:

```
-- переключиться на ветку л/р
git checkout <фамилия>_lab_<номер л/р>_<вариант>

-- изменить, закоммитить новые изменения
git add .
git commit -m "Мои исправления"

-- сохранить их в репозиторий
git push origin
```

Если все сделали верно, то изменения появятся в рамках существующего мерж-реквеста.

5. Если все хорошо, то я вмерживаю мерж-реквест в ветку main - это и считается сданной лабораторной работой! Ура!

## P.S.

1. Пожалуйста, если есть какой-либо вопрос, касаемый предмета, то пожалуйста пишите его в **группу в Telegram**. В личке я могу не ответить (сообщение потеряется), так как в Telegram, похвастаюсь, у меня 2000 контактов примерно, из которых 150+ для меня сейчас активны.

2. Лабораторные работы я обычно проверяю по вечерам после основной работы. А работаю я вот тут https://www.ecwid.ru/dev, если интересно присоединиться - скажите мне!

3. Каждую субботу я буду проводить созвон, о времени буду сообщать в группе в Telegram, если есть какие-то вопросы по выполнению лабораторных работ. На нем также можно задать вопрос (либо в чате в любое время дня и ночи).

4. Все созвоны проходят в сервисе Google Meet.

