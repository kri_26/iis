import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np

data = pandas.read_csv('titanic.csv', index_col='PassengerId')


def sex_to_bool(sex):
    if sex == "male":
        return 0
    return 1


data['Sex'] = data['Sex'].apply(sex_to_bool)


def name_to_bool(name):
    return 0


data['Name'] = data['Name'].apply(name_to_bool)


def non_empty_data(data):
    if data.loc[np.isnan(data) == False]:
        return 0
    return 1


corr = data[['Pclass', 'Name', 'Sex']]

print(corr.head())

y = data['Survived']

clf = DecisionTreeClassifier(random_state=241)
clf.fit(corr, y)

importances = clf.feature_importances_
print(importances)
