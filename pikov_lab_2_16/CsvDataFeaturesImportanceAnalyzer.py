import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier

FEATURE_IMPORTANCE_PAIR_INDEX = 1


class CsvDataFeaturesImportanceAnalyzer:

    def __init__(self, csv_file_name, researchable_column_names, target_column_name):
        table = get_prepared_table_from_file(csv_file_name)

        self._researchable_columns = table[researchable_column_names]
        self._target_column = table[target_column_name]

        self._model = self.create_model()

    def create_model(self):
        model = DecisionTreeClassifier()
        model.fit(self._researchable_columns, self._target_column)
        return model

    def print_features_importance(self):
        sorted_feature_name_to_importance = self.get_sorted_features_importance()
        print_features_importance(sorted_feature_name_to_importance)

    def get_sorted_features_importance(self):
        feature_name_to_importance = zip(
            self._researchable_columns,
            self._model.feature_importances_
        )
        return sort_by_feature_importance_desc(feature_name_to_importance)


def get_prepared_table_from_file(csv_file_name):
    table = pd.read_csv(csv_file_name)
    table = replace_strings_with_integers(table)
    return table


def replace_strings_with_integers(table):
    return table.apply(LabelEncoder().fit_transform)


def sort_by_feature_importance_desc(feature_name_to_importance):
    return sorted(
        feature_name_to_importance,
        key=lambda pair: pair[FEATURE_IMPORTANCE_PAIR_INDEX],
        reverse=True
    )


def print_features_importance(feature_name_to_importance):
    print("\nFeatures importance:")
    for feature_name, importance in feature_name_to_importance:
        print("%s importance: %.5f" % (feature_name, importance))
