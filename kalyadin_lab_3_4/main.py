import numpy as np
from sklearn.datasets import make_moons
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures


def linear_regression(x_learn, x_test, y_learn, y_test):
    model = LinearRegression().fit(x_learn, y_learn)
    y_pred = model.predict(x_test)
    print(model.score(x_learn, y_learn))
    plt.title("linear_regression")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='blue', linewidth=2)
    plt.show()


def polynomial_regression(x, y):
    poly_reg = PolynomialFeatures(degree=4)
    x_poly = poly_reg.fit_transform(x)
    pol_reg = LinearRegression()
    pol_reg.fit(x_poly, y)
    print(pol_reg.score(x_poly, y))
    plt.title("polynomial_regression")
    plt.scatter(x, y, color='red')
    plt.plot(x, pol_reg.predict(poly_reg.fit_transform(x)), color='red', linewidth=2)
    plt.show()


def ridge_regression(x_ridge, x_test, y_ridge, y_test):
    clf = Ridge(alpha=1.0)
    clf.fit(x_ridge, y_ridge)
    y_pred = clf.predict(x_test)
    print(clf.score(x_ridge, y_ridge))
    plt.title("ridge_regression")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='green', linewidth=2)
    plt.show()


def get_dataset():
    return make_moons(noise=0.2, random_state=1)


def get_data(x, y):
    X1 = x[:75]
    X2 = x[75:]
    Y1 = y[:75]
    Y2 = y[75:]
    return X1, X2, Y1, Y2


def call_regression():
    linear_regression(X1, X2, Y1, Y2)
    polynomial_regression(X1, Y1)
    ridge_regression(X1, X2, Y1, Y2)


dataset = get_dataset()
print(dataset)
x = np.array(dataset[0])
y = np.array(dataset[1])
x = np.delete(x, 0, 1)

X1, X2, Y1, Y2 = get_data(x, y)

call_regression()
