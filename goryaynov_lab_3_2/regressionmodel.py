from abc import ABCMeta, abstractmethod

class RegressionModel:

    __metaclass__ = ABCMeta

    @abstractmethod
    def run(self, x_train, y_train, x_test, y_test):
        pass
