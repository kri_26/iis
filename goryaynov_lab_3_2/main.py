from typing import List
import numpy as np
from sklearn.datasets import make_circles
from regressionmodel import RegressionModel
from linearmodel import LinearModel
from polynomialmodel import PolynomialModel
from ridgemodel import RidgeModel

x, y = make_circles(noise=0.2, factor=0.5, random_state=1)
x = np.delete(x, 1, 1)
x_train = x[:-10]
x_test = x[-10:]
y_train = y[:-10]
y_test = y[-10:]
models: List[RegressionModel] = [
    LinearModel(), 
    PolynomialModel(), 
    RidgeModel()
]
for model in models:
    model.run(x_train, y_train, x_test, y_test)