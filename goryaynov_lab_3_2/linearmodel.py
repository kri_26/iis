from regressionmodel import RegressionModel
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from matplotlib import pyplot as plt

class LinearModel(RegressionModel):

    def run(self, x_train, y_train, x_test, y_test):
        model = LinearRegression().fit(x_train, y_train)
        y_pred = model.predict(x_test)
        plt.scatter(x_test, y_test, color='black')
        plt.plot(x_test, y_pred, color='blue', linewidth=3)
        plt.show()
        print('---Линейная регрессия---')
        print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_test, y_pred))
        print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_test, y_pred))
        