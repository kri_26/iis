import pandas as pd

DATA_FILE_NAME = 'titanic.csv'
IS_SURVIVED_COLUMN_NAME = 'Survived'
FARE_COLUMN_NAME = 'Fare'


def main():
    dead_passengers_fare_sum = calculate_fare_sum_of_dead_passengers(
        data_filename=DATA_FILE_NAME
    )
    print('16. Какова суммарная стоимость билетов у погибших? Ответ: %s' % dead_passengers_fare_sum)


def calculate_fare_sum_of_dead_passengers(data_filename):
    passengers_table = pd.read_csv(data_filename)

    dead_passengers = select_dead_passengers_from(passengers_table)
    fare_sum_of_dead_passengers = calculate_fare_sum_for(dead_passengers)

    return fare_sum_of_dead_passengers


def select_dead_passengers_from(passengers):
    is_survived = passengers[IS_SURVIVED_COLUMN_NAME]
    dead_passengers_selection = passengers[is_survived == False]
    return dead_passengers_selection


def calculate_fare_sum_for(passengers):
    fair = passengers[FARE_COLUMN_NAME]
    fare_sum = fair.sum()
    return fare_sum


if __name__ == "__main__":
    main()
