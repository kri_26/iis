
# make_moons (noise=0.3, random_state=rs) 
# Линейная регрессия
# Полиномиальную регрессия(со степенью 5) 
# Гребневую полиномиальную регрессия (со степенью 5, alpha = 1.0) 

import numpy as np
from sklearn.datasets import make_moons
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures


def linear(x_train, x_test, y_train, y_test):
    model = LinearRegression().fit(x_train, y_train)
    y_pred = model.predict(x_test)
    print('Линейный коэффициент детерминации:', model.score(x_train, y_train))
    plt.title("Линейная регрессия")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='red', linewidth=3)
    plt.show()


def polynomial(x, y):
    poly_reg = PolynomialFeatures(degree=5)
    x_poly = poly_reg.fit_transform(x)
    pol_reg = LinearRegression()
    pol_reg.fit(x_poly, y)
    print('Полиномиальный коэффициент детерминации:', pol_reg.score(x_poly, y))
    plt.title("Полиномиальная регрессия")
    plt.scatter(x, y, color='red')
    plt.plot(x, pol_reg.predict(poly_reg.fit_transform(x)), color='blue', linewidth=3)
    plt.show()


def ridge(x_train, x_test, y_train, y_test):
    clf = Ridge(alpha=1.0)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)
    print('Гребневой коэффициент детерминации:', clf.score(x_train, y_train))
    plt.title("Гребневая полиномиальная регрессия")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='red', linewidth=3)
    plt.show()


dataset = make_moons(noise=0.3, random_state=1)
print(dataset)

x = np.array(dataset[0])
y = np.array(dataset[1])

# Преобразуем двумерный массив в одномерный
x = x[:, np.newaxis, 1]

# 50 значений для тренировки, 50 для теста
x_train, x_test = x[:-50], x[-50:]
y_train, y_test = y[:-50], y[-50:]

linear(x_train, x_test, y_train, y_test)
polynomial(x_train, y_train)
ridge(x_train, x_test, y_train, y_test)
