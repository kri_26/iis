'''
По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений), 
в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров
два наиболее важных признака из трех рассматриваемых (по варианту - Age, SibSp, Parch).
'''

import pandas
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree 
import matplotlib.pyplot as pyplot
from typing import Final

AGE_COL_NAME: Final = 'Age'
SIBSP_COL_NAME: Final = 'SibSp'
PARCH_COL_NAME: Final = 'Parch'
SURVIVED_COL_NAME: Final = 'Survived'

def read_dataset(path: str) -> DataFrame:
    return pandas.read_csv(path, index_col='PassengerId')

def drop_empty_values(dataframe):
    return dataframe[[AGE_COL_NAME, SIBSP_COL_NAME, PARCH_COL_NAME, SURVIVED_COL_NAME]].dropna()

data = read_dataset('titanic.csv')
dataframe = pandas.DataFrame(data=data, columns=[AGE_COL_NAME, SIBSP_COL_NAME, PARCH_COL_NAME, SURVIVED_COL_NAME])
dataframe = drop_empty_values(dataframe)
classifier = DecisionTreeClassifier()
# выбираем данные
y = dataframe[SURVIVED_COL_NAME]
x = dataframe[[AGE_COL_NAME, SIBSP_COL_NAME, PARCH_COL_NAME]]
# обучаем модель
classifier.fit(x, y)

# выводим названия признаков и их важность 
print(classifier.feature_names_in_)
print(classifier.feature_importances_)
tree.plot_tree(classifier)
pyplot.show()